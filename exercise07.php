<?php

/* Функция принимает 1 строковый параметр и возвращает TRUE, если строка является палиндромом, FALSE в противном случае. */

error_reporting(-1);

function isPalindrom($str) {

    $str = mb_strtolower($str, 'UTF-8');

    $badSymbols = array(' ', ',', '.', '-', '!', '?');  // символы которые нужно удалить из строки

    $str = str_replace($badSymbols, "", $str);          // заменяем не нужные символы пустой строкой

    $arrFromStr = preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);      // функция для разбиения строки в массив, подсмотрена с php.net

    $revArrFromStr = array_reverse($arrFromStr);

    $revString = implode($revArrFromStr);

    if($str == $revString) return true;
    else return false;
}


var_dump(isPalindrom('Коту скоро сорок суток.'));
var_dump(isPalindrom('А роза упала на лапу азора.'));