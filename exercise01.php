<?php

    /* Функция должна принимать массив строк и выводить каждую строку в
    отдельном параграфе. */

function somefunc($arr) {
    foreach($arr as $str) {
        echo "<p>$str</p>";
    }
}

$arr = array('Жили у бабуси два весёлых гуся', 'Один серый другой белый', 'Два весёлых гуся');
somefunc($arr);

    /* Функция должна принимать массив строк и выводить каждую строку в отдельном пункте списка */

function ingredients($ingr) {
    echo '<ol>';
    foreach($ingr as $str) {
        echo "<li>$str</li>";
    }
    echo '</ol>';
}

$ingr = array('Колбаса', 'Картофель','Горошек','Яйца','Майонез');
ingredients($ingr);