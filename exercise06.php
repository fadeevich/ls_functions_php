<?php

/* Создать рекурсивную функцию, которая принимает два целых числа, начальное и конечное значения, диапазон. Функция должна вывести список нечётных чисел в заданном диапазоне. */

error_reporting(-1);


function oddNumbersInRange($a, $b) {
        if ($a < $b) {
            if ($a % 2 != 0) {
                echo "$a ";
            }
            $a++;
            return oddNumbersInRange($a, $b);
        } else return;
}

oddNumbersInRange(10, 35);
echo '<br>';

/* Своя задача. Создать рекурсивную функцию вычисляющую степень числа */

function exponent($x, $n) {
    if($n != 1) {
        $n--;
        return $x * exponent($x, $n);
    } else return $x;
}

$res = exponent(2, 8);
echo "$res";
