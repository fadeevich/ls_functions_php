<?php

/* Функция должна принимать переменное число аргументов, но первым аргументом обязательно должна быть строка, обозначающая арифметическое действие,
которое необходимо выполнить со всеми передаваемыми аргументами. Остальные аргументы целые и/или вещественные. */

error_reporting(-1);

function calculator($operator) {

    $numbers = array();

    for($i = 1; $i < func_num_args(); $i++) {
        $numbers[] = func_get_arg($i);
    }

    $result = $numbers[0];
    $result_string = $numbers[0];

    for($i = 1; $i < count($numbers); $i++) {
        $result_string .=" $operator $numbers[$i]";
    }

    switch($operator) {
        case '+':
            for($i = 1; $i < count($numbers); $i++) {
                $result += $numbers[$i];
            }
            break;
        case '-':
            for($i = 1; $i < count($numbers); $i++) {
                $result -= $numbers[$i];
            }
            break;
        case '*':
            for($i = 1; $i < count($numbers); $i++) {
                $result *= $numbers[$i];
            }
            break;
        case '/':
            for($i = 1; $i < count($numbers); $i++) {
                $result /= $numbers[$i];
            }
            break;
    }

    echo "$result_string = $result";
}

calculator('*', 1, 2, 3, 4);