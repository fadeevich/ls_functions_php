<?php

/* Функция должна принимать 2 параметра: а) массив чисел; б) строку, обозначающую арифметическое действие, которое нужно выполнить со всеми элементами массива.
Функция должна вывести результат. */

error_reporting(-1);

function calculator($numbers, $operator) {

    $result = $numbers[0];
    $result_string = $numbers[0];

    for($i = 1; $i < count($numbers); $i++) {
        $result_string .=" $operator $numbers[$i]";
    }

    switch($operator) {
        case '+':
            for($i = 1; $i < count($numbers); $i++) {
                $result += $numbers[$i];
            }
            break;
        case '-':
            for($i = 1; $i < count($numbers); $i++) {
                $result -= $numbers[$i];
            }
            break;
        case '*':
            for($i = 1; $i < count($numbers); $i++) {
                $result *= $numbers[$i];
            }
            break;
        case '/':
            for($i = 1; $i < count($numbers); $i++) {
                $result /= $numbers[$i];
            }
            break;
    }

    echo "$result_string = $result";
}

calculator(array(1, 2, 3, 4, 5), '*');