<?php

/* Своя задача. Функция должна принимать переменное число аргументов как в задачах 2 и 3, и в зависимости от того что пришло действовать. */

error_reporting(-1);

function calculator() {

    $arguments = func_get_args();

    if(is_array($arguments[0]) AND is_string($arguments[1])) {
        $numbers = $arguments[0];
        $operator = $arguments[1];
    } elseif(is_string($arguments[0])) {
        $numbers = array();
        $operator = $arguments[0];
        for($i = 1; $i < func_num_args(); $i++) {
            $numbers[] = func_get_arg($i);
        }
    } else echo "Не правильно переданные параметры, см.задачу 2 и 3";

    $result = $numbers[0];
    $result_string = $numbers[0];

    for($i = 1; $i < count($numbers); $i++) {
        $result_string .=" $operator $numbers[$i]";
    }

    switch($operator) {
        case '+':
            for($i = 1; $i < count($numbers); $i++) {
                $result += $numbers[$i];
            }
            break;
        case '-':
            for($i = 1; $i < count($numbers); $i++) {
                $result -= $numbers[$i];
            }
            break;
        case '*':
            for($i = 1; $i < count($numbers); $i++) {
                $result *= $numbers[$i];
            }
            break;
        case '/':
            for($i = 1; $i < count($numbers); $i++) {
                $result /= $numbers[$i];
            }
            break;
    }

    echo "$result_string = $result";
}

calculator(array(1, 2, 3, 4, 5), '*');
echo '<br>';
calculator('*', 1, 2, 3, 4);