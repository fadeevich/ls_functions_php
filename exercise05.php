<?php

/* Функция должна принимать в качестве аргумента массив чисел и возвращать так же массив, но отсортированный по возрастанию. */
// сортировка пузырьком
function increase($arr) {
    $count = count($arr);
    for ($j = 0; $j < $count - 1; $j++){
        for ($i = 0; $i < $count - $j - 1; $i++){
            if ($arr[$i] > $arr[$i + 1]){
                $temp = $arr[$i + 1];
                $arr[$i + 1] = $arr[$i];
                $arr[$i] = $temp;
            }
        }
    }
    return $arr;
}

$arr = array(1, 22, 5, 66, 3, 57);
$sort_arr = increase($arr);
print_r($sort_arr);

/* Своя задача. Функция должна принимать в качестве аргумента массив чисел и возвращать так же массив, но отсортированный по убыванию. */

function decrease($arr) {
    $count = count($arr);
    for ($j = 0; $j < $count - 1; $j++){
        for ($i = 0; $i < $count - $j - 1; $i++){
            if ($arr[$i] < $arr[$i + 1]){
                $temp = $arr[$i + 1];
                $arr[$i + 1] = $arr[$i];
                $arr[$i] = $temp;
            }
        }
    }
    return $arr;
}

$arr = array(1, 22, 5, 66, 3, 57);
$sort_arr = decrease($arr);
print_r($sort_arr);